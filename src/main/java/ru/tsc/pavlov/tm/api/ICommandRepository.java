package ru.tsc.pavlov.tm.api;

import ru.tsc.pavlov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
